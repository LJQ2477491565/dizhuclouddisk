/*
SQLyog Ultimate v10.00 Beta1
MySQL - 8.0.29 : Database - doudizhu_cloud_disk
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`doudizhu_cloud_disk` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
DROP DATABASE IF EXISTS doudizhu_cloud_disk;
CREATE DATABASE IF NOT EXISTS doudizhu_cloud_disk;

USE `doudizhu_cloud_disk`;

/*Table structure for table `file` */

DROP TABLE IF EXISTS `file`;

CREATE TABLE `file` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `file_name` varchar(100) CHARACTER SET ucs2 NOT NULL COMMENT '文件名称',
  `file_type` varchar(10) CHARACTER SET ucs2 NOT NULL DEFAULT '""' COMMENT '文件类型',
  `file_size` int unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  `file_location` varchar(1000) CHARACTER SET ucs2 COLLATE ucs2_general_ci NOT NULL COMMENT '文件位置',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modifier_id` int DEFAULT NULL COMMENT '修改者id',
  `creator_id` int DEFAULT NULL COMMENT '创建者id',
  `file_md5` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '""' COMMENT '文件Md5',
  `file_extension` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '""' COMMENT '文件后缀',
  `is_dir` int NOT NULL DEFAULT '0' COMMENT '是否为文件夹',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=146 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `file` */

/*Table structure for table `file_share` */

DROP TABLE IF EXISTS `file_share`;

CREATE TABLE `file_share` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `share_md5` varchar(1000) NOT NULL COMMENT '共享文件md5',
  `file_id` int NOT NULL COMMENT '共享文件id',
  `expiration_time` datetime NOT NULL COMMENT '过期时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modifier_id` int DEFAULT NULL COMMENT '修改者id',
  `creator_id` int DEFAULT NULL COMMENT '创建者id',
  `share_uuid` varchar(20) NOT NULL COMMENT '分享uuid',
  `share_location` varchar(1000) NOT NULL DEFAULT '""' COMMENT '文件夹分享压缩地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `file_share` */

/*Table structure for table `recycle_bin` */

DROP TABLE IF EXISTS `recycle_bin`;

CREATE TABLE `recycle_bin` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `file_name` varchar(100) NOT NULL COMMENT '文件名称',
  `file_type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '""' COMMENT '文件类型',
  `file_size` int NOT NULL DEFAULT '0' COMMENT '文件大小',
  `file_location` varchar(1000) NOT NULL COMMENT '文件位置',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modifier_id` int DEFAULT NULL COMMENT '修改者id',
  `creator_id` int DEFAULT NULL COMMENT '创建者id',
  `file_md5` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '""' COMMENT '文件Md5',
  `file_extension` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '""' COMMENT '文件后缀',
  `file_expiration_time` datetime NOT NULL COMMENT '文件保留截止时期',
  `is_dir` int NOT NULL DEFAULT '0' COMMENT '是否为文件夹',
  `root` int NOT NULL DEFAULT '0' COMMENT '是否为根目录',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `recycle_bin` */

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `username` varchar(20) NOT NULL COMMENT '用户名',
  `email` varchar(20) NOT NULL COMMENT '邮箱',
  `password` varchar(20) DEFAULT NULL COMMENT '密码',
  `role` varchar(10) DEFAULT NULL COMMENT '角色权限',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modifier_id` int DEFAULT NULL COMMENT '创建者id',
  `creator_id` int DEFAULT NULL COMMENT '修改者id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `user` */

/*Table structure for table `user_space` */

DROP TABLE IF EXISTS `user_space`;

CREATE TABLE `user_space` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `use_space` int NOT NULL COMMENT '使用空间',
  `free_space` int NOT NULL COMMENT '剩余空间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modifier_id` int DEFAULT NULL COMMENT '修改者id',
  `creator_id` int DEFAULT NULL COMMENT '创建者id',
  `user_id` int NOT NULL COMMENT '用户id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `user_space` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
