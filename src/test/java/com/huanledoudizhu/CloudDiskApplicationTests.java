package com.huanledoudizhu;

import com.huanledoudizhu.entity.User;
import com.huanledoudizhu.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class CloudDiskApplicationTests {
    @Autowired
    UserService userService;

    @Test
    void contextLoads() {
        User user = new User();
        user.setEmail("2477491565@qq.com");
        userService.sendCodeToEmail(user,null);
    }

}
