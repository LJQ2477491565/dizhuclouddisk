package com.huanledoudizhu.config;

import com.huanledoudizhu.filter.LoginInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

// 未登录不可访问
@Configuration
public class WebConfigurer implements WebMvcConfigurer {
    @Autowired
    private LoginInterceptor loginInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 未登录不可访问
        registry.addInterceptor(loginInterceptor).excludePathPatterns("/user/login", "/user/register", "/user/email","/user/emailLogin", "/v3/api-docs", "/swagger-resources/**", "/swagger-ui/**");
    }
}
