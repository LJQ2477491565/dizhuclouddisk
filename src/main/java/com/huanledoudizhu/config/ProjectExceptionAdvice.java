package com.huanledoudizhu.config;

import com.huanledoudizhu.controller.result.ResponseResult;
import com.huanledoudizhu.controller.result.ResultCode;
import com.sun.mail.smtp.SMTPSendFailedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.mail.SendFailedException;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;


@RestControllerAdvice()
public class ProjectExceptionAdvice {
    @ExceptionHandler(Exception.class)
    public ResponseResult doException(Exception ex){
        System.out.println("嘿嘿，出错了，别急");
        System.out.println(ex);
        return new ResponseResult(ResultCode.SYSTEM_UNKNOW_ERR);
    }
}
