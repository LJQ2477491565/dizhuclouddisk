package com.huanledoudizhu.service;

import com.huanledoudizhu.controller.result.ResponseResult;
import com.huanledoudizhu.entity.RecycleBin;
import com.baomidou.mybatisplus.extension.service.IService;
import org.json.JSONException;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author huanledoudizhu
 * @since 2022-11-13
 */
public interface RecycleBinService extends IService<RecycleBin> {
    @Transactional(rollbackFor = Exception.class)
    ResponseResult deleteToRecycleBin(List<Integer> checkbox) throws JSONException;
    @Transactional(rollbackFor = Exception.class)
    ResponseResult recoverFromRecycleBin(List<Integer> checkbox);
    @Transactional(rollbackFor = Exception.class)
    ResponseResult getDirectory();
    @Transactional(rollbackFor = Exception.class)
    ResponseResult delete(List<Integer> checkbox);
}
