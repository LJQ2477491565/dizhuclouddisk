package com.huanledoudizhu.service;

import com.huanledoudizhu.controller.result.ResponseResult;
import com.huanledoudizhu.entity.File;
import com.baomidou.mybatisplus.extension.service.IService;
import org.json.JSONException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author huanledoudizhu
 * @since 2022-11-13
 */
public interface FileService extends IService<File> {
    @Transactional(rollbackFor = Exception.class)
    ResponseResult upload(MultipartFile file, String currentDirectory) throws IOException;
    @Transactional(rollbackFor = Exception.class)
    ResponseResult download(int id, HttpServletResponse httpServletResponse, HttpServletRequest httpServletRequest) throws IOException;
    @Transactional(rollbackFor = Exception.class)
    ResponseResult delete(List<Integer> checkbox) throws JSONException;
    @Transactional(rollbackFor = Exception.class)
    ResponseResult createDir(String dirName, String currentDirectory);
    @Transactional(rollbackFor = Exception.class)
    ResponseResult modifyFileName(int id, String fileName, String currentDirectory) throws IOException;
    @Transactional(rollbackFor = Exception.class)
    ResponseResult currentDirectory(String currentDirectory);
    @Transactional(rollbackFor = Exception.class)
    ResponseResult classification(String s);
}