package com.huanledoudizhu.service;

import com.huanledoudizhu.controller.result.ResponseResult;
import com.huanledoudizhu.entity.FileShare;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author huanledoudizhu
 * @since 2022-11-21
 */
public interface FileShareService extends IService<FileShare> {
    @Transactional(rollbackFor = Exception.class)
    ResponseResult generateLink(int id, Date expirationTime) throws IOException;
    @Transactional(rollbackFor = Exception.class)
    ResponseResult linkDownload(String uuid, HttpServletResponse httpServletResponse, HttpServletRequest httpServletRequest) throws IOException;
}
