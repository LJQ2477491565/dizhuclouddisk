package com.huanledoudizhu.service;

import com.huanledoudizhu.entity.UserSpace;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author huanledoudizhu
 * @since 2022-11-13
 */
public interface UserSpaceService extends IService<UserSpace> {

}