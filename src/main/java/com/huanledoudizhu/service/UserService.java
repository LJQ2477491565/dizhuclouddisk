package com.huanledoudizhu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huanledoudizhu.controller.result.ResponseResult;
import com.huanledoudizhu.entity.User;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author huanledoudizhu
 * @since 2022-11-13
 */
public interface UserService extends IService<User> {
    @Transactional(rollbackFor = Exception.class)
    ResponseResult login(User user, BindingResult bindingResult);
    @Transactional(rollbackFor = Exception.class)
    ResponseResult sendCodeToEmail(User user, BindingResult bindingResult);
    @Transactional(rollbackFor = Exception.class)
    ResponseResult verification(String email, int code);
    @Transactional(rollbackFor = Exception.class)
    ResponseResult emailLogin(User user, int code);
    @Transactional(rollbackFor = Exception.class)
    ResponseResult register(User user, int code, BindingResult bindingResult);
}
