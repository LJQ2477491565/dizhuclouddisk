package com.huanledoudizhu.service.impl;

import cn.dev33.satoken.stp.StpUtil;
import com.huanledoudizhu.controller.result.ResponseResult;
import com.huanledoudizhu.controller.result.ResultCode;
import com.huanledoudizhu.entity.File;
import com.huanledoudizhu.entity.FileShare;
import com.huanledoudizhu.entity.RecycleBin;
import com.huanledoudizhu.mapper.FileMapper;
import com.huanledoudizhu.mapper.RecycleBinMapper;
import com.huanledoudizhu.service.FileService;
import com.huanledoudizhu.service.RecycleBinService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author huanledoudizhu
 * @since 2022-11-13
 */
@Service
public class RecycleBinServiceImpl extends ServiceImpl<RecycleBinMapper, RecycleBin> implements RecycleBinService {
    @Autowired
    FileService fileService;

    @Autowired
    FileMapper fileMapper;

    @Autowired
    RecycleBinMapper recycleBinMapper;

    public ResponseResult deleteToRecycleBin(List<Integer> checkbox){
        for (Integer integer : checkbox) {
            File file = fileService.getById(integer);
            if (file.getIsDir()==1){
                deleteDir(file);
            }else {
                fileMapper.deleteById(integer);
                Date date = new Date(new Date().getTime()+864000000);
                LocalDateTime localDateTime = LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
                RecycleBin recycleBin = new RecycleBin(file,localDateTime,1);
                this.save(recycleBin);
            }
            fileMapper.deleteById(integer);
        }
        return new ResponseResult(ResultCode.SUCESS);
    }

    public ResponseResult recoverFromRecycleBin(List<Integer> checkbox){
        for (Integer integer : checkbox) {
            RecycleBin recycleBin = recycleBinMapper.getById(integer);
            if (recycleBin.getRoot()==0){
                int error = 1/0;
            }
            if (recycleBin.getIsDir()==1){
                recoverDir(recycleBin);
            }else {
                recycleBinMapper.deleteById(integer);
                File file = new File(recycleBin);
                fileService.save(file);
            }

            recycleBinMapper.deleteById(integer);
        }
        return new ResponseResult(ResultCode.SUCESS);
    }

    public void deleteDir(File dir){
        List<File> fileList = fileMapper.getByCreatorId(Integer.parseInt((String)StpUtil.getLoginId()));
        for (File file : fileList) {
            String l = file.getFileLocation();
            if (l.contains(dir.getFileLocation())&&(l.substring(dir.getFileLocation().length()).equals("") || l.substring(dir.getFileLocation().length()).charAt(0)=='\\')) {
                fileMapper.deleteById(file.getId());
                Date date = new Date(new Date().getTime()+864000000);
                LocalDateTime localDateTime = LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
                if (file.getIsDir()==1&&l.equals(dir.getFileLocation())){
                    RecycleBin recycleBin = new RecycleBin(file,localDateTime, 1);
                    this.save(recycleBin);
                }else {
                    RecycleBin recycleBin = new RecycleBin(file,localDateTime, 0);
                    this.save(recycleBin);

                }
            }
        }
    }

    public void recoverDir(RecycleBin dir){
        List<RecycleBin> recycleBinList = recycleBinMapper.getByCreatorId(Integer.parseInt((String)StpUtil.getLoginId()));
        for (RecycleBin recycleBin : recycleBinList) {
            String l = recycleBin.getFileLocation();
            if (l.contains(dir.getFileLocation())&&(l.substring(dir.getFileLocation().length()).equals("") || l.substring(dir.getFileLocation().length()).charAt(0)=='\\')) {
                recycleBinMapper.deleteById(recycleBin.getId());
                File file = new File(recycleBin);
                fileService.save(file);
            }
        }
    }


    public ResponseResult getDirectory(){
        List<RecycleBin> recycleBinList = recycleBinMapper.getByCreatorId(Integer.parseInt((String)StpUtil.getLoginId()));
        List<RecycleBin> recycleBinList1 = new ArrayList<>();
        for (RecycleBin recycleBin : recycleBinList) {
            if (recycleBin.getRoot()==1){
                recycleBinList1.add(recycleBin);
            }
        }
        return new ResponseResult(ResultCode.SUCESS,recycleBinList1);
    }


    public ResponseResult delete(List<Integer> checkbox){
        for (Integer integer : checkbox) {
            RecycleBin recycleBin = this.getById(integer);
            System.out.println(integer);
            if (recycleBin.getRoot()==1){
                if (recycleBin.getIsDir() == 1) {
                    deleteDirRecycle(integer, Integer.parseInt((String) StpUtil.getLoginId()), recycleBin);//删除回收站这个文件夹下的所有子文件
                    java.io.File f = new java.io.File(recycleBin.getFileLocation());
                    deleteDir(f);//物理删除
                } else {
                    recycleBinMapper.deleteByIdWhereCreatorId(integer, Integer.parseInt((String) StpUtil.getLoginId()));//删除本文件
                    java.io.File f = new java.io.File(recycleBin.getFileLocation(), recycleBin.getFileName() + "." + recycleBin.getFileExtension());
                    f.delete();
                }
            }
        }
        return new ResponseResult(ResultCode.SUCESS);
    }

    public void deleteDir(java.io.File dir){
        if (dir.isDirectory()){
            String[] children = dir.list();
            for (int i = 0; i < children.length;i++){
                deleteDir(new java.io.File(dir,children[i]));
            }
        }
        dir.delete();
    }

    public void deleteDirSql(int id, int userId, File dir){
        List<File> fileList = fileMapper.getByCreatorId(userId);
        for (File file : fileList) {
            String l = file.getFileLocation();
            if (l.contains(dir.getFileLocation())&&(l.substring(dir.getFileLocation().length()).equals("") || l.substring(dir.getFileLocation().length()).charAt(0)=='\\')) {
                fileMapper.deleteById(file.getId());
            }
        }
    }

    public void deleteDirRecycle(int id, int userId, RecycleBin dir){
        List<RecycleBin> recycleBinList = recycleBinMapper.getByCreatorId(userId);
        for (RecycleBin recycleBin : recycleBinList) {
            String l = recycleBin.getFileLocation();
            if (l.contains(dir.getFileLocation())&&(l.substring(dir.getFileLocation().length()).equals("") || l.substring(dir.getFileLocation().length()).charAt(0)=='\\')) {
                recycleBinMapper.deleteById(recycleBin.getId());
            }
        }
    }

}
