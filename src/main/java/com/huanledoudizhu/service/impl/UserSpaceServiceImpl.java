package com.huanledoudizhu.service.impl;

import com.huanledoudizhu.entity.UserSpace;
import com.huanledoudizhu.mapper.UserSpaceMapper;
import com.huanledoudizhu.service.UserSpaceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author huanledoudizhu
 * @since 2022-11-13
 */
@Service
public class UserSpaceServiceImpl extends ServiceImpl<UserSpaceMapper, UserSpace> implements UserSpaceService {

}
