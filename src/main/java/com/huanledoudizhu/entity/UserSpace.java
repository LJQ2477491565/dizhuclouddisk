package com.huanledoudizhu.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.huanledoudizhu.config.BaseEntity;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author huanledoudizhu
 * @since 2022-11-13
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value="UserSpace对象", description="")
public class UserSpace extends BaseEntity<UserSpace> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "使用空间")
    private Integer useSpace;

    @ApiModelProperty(value = "剩余空间")
    private Integer freeSpace;

    @ApiModelProperty(value = "用户id")
    private Integer userId;


    @Override
    protected Serializable pkVal() {
        return null;
    }

}
