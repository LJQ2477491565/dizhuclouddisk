package com.huanledoudizhu.entity.bean;

import java.util.Arrays;
import java.util.List;

public interface Consts {

    /**
     * 文件后缀
     */
    //文档
    List<String> document= Arrays.asList(".doc",".docx",".xls",".xlsx",".ppt",".pptx",".txt",".pdf");
    //图片
    List<String> picture=Arrays.asList(".jpg",".png",".JPEG",".bmp",".gif");
    //视屏
    List<String> video=Arrays.asList(".avi",".mp4",".rmvb",".flv",".wmv");
    //音频
    List<String> audio=Arrays.asList(".mp3",".wav",".amr");
    //压缩
    List<String> compress=Arrays.asList(".zip",".rar",".7z");
}