package com.huanledoudizhu.entity.bean;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.Date;

public class VerificationCode{
    int code;//验证码
    long date;//过期时间戳
    String email;//被验证邮箱

    public VerificationCode(int code, long date, @Email(message = "邮箱格式不正确！") @NotBlank(message = "邮箱不能为空！") String email) {
        this.code = code;
        this.date = date;
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }
}