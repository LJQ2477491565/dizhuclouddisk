package com.huanledoudizhu.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.huanledoudizhu.config.BaseEntity;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * <p>
 * 
 * </p>
 *
 * @author huanledoudizhu
 * @since 2022-11-13
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value="User对象", description="")
public class User extends BaseEntity<User> {

    private static final long serialVersionUID = 1L;

    @NotBlank
    @Pattern(regexp = "[\u4e00-\u9fa5]{2,6}",message = "用户名：请输入2-6位中文")
    @ApiModelProperty(value = "用户名")
    private String username;

    @Email(message = "邮箱格式不正确！")
    @NotBlank(message = "邮箱不能为空！")
    @ApiModelProperty(value = "邮箱")
    private String email;

    @NotBlank(message = "密码不能为空！")
    @Pattern(regexp = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{8,24}$", message = "密码：8-24位由字母和数字")
    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "角色权限")
    @Pattern(regexp = "\\d", message = "权限：1位数字！")
    private String role;

    @Override
    protected Serializable pkVal() {
        return null;
    }

}
