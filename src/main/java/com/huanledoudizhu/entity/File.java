package com.huanledoudizhu.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.huanledoudizhu.config.BaseEntity;
import java.io.Serializable;
import java.time.LocalDateTime;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author huanledoudizhu
 * @since 2022-11-13
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value="File对象", description="")
public class File extends BaseEntity<File> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "文件名称")
    private String fileName;

    @ApiModelProperty(value = "文件类型")
    private String fileType;

    @ApiModelProperty(value = "文件大小")
    private Integer fileSize;

    @ApiModelProperty(value = "文件位置")
    private String fileLocation;

    @ApiModelProperty(value = "文件Md5")
    private String fileMd5;

    @ApiModelProperty(value = "文件后缀")
    private String fileExtension;

    @ApiModelProperty(value = "是否为文件夹")
    private Integer isDir;

    @Override
    protected Serializable pkVal() {
        return null;
    }

    public File(RecycleBin recycleBin) {
        this.fileName = recycleBin.getFileName();
        this.fileType = recycleBin.getFileType();
        this.fileSize = recycleBin.getFileSize();
        this.fileLocation = recycleBin.getFileLocation();
        this.fileMd5 = recycleBin.getFileMd5();
        this.fileExtension = recycleBin.getFileExtension();
        this.isDir = recycleBin.getIsDir();
    }

    public File(){
        super();
    }
}
