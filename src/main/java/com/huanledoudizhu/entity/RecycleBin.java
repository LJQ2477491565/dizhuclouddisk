package com.huanledoudizhu.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import com.huanledoudizhu.config.BaseEntity;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author huanledoudizhu
 * @since 2022-11-13
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value="RecycleBin对象", description="")
public class RecycleBin extends BaseEntity<RecycleBin> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "文件名称")
    private String fileName;

    @ApiModelProperty(value = "文件类型")
    private String fileType;

    @ApiModelProperty(value = "文件大小")
    private Integer fileSize;


    @ApiModelProperty(value = "文件位置")
    private String fileLocation;

    @ApiModelProperty(value = "文件Md5")
    private String fileMd5;

    @ApiModelProperty(value = "文件后缀")
    private String fileExtension;

    @ApiModelProperty(value = "文件保留截止时期")
    private LocalDateTime fileExpirationTime;

    @ApiModelProperty(value = "是否为文件夹")
    private Integer isDir;

    @ApiModelProperty(value = "是否为根目录")
    private Integer root;

    @Override
    protected Serializable pkVal() {
        return null;
    }

    public RecycleBin(File file, LocalDateTime fileExpirationTime, int root) {
        this.fileName = file.getFileName();
        this.fileType = file.getFileType();
        this.fileSize = file.getFileSize();
        this.fileLocation = file.getFileLocation();
        this.fileMd5 = file.getFileMd5();
        this.fileExtension = file.getFileExtension();
        this.fileExpirationTime = fileExpirationTime;
        this.isDir = file.getIsDir();
        this.root = root;
    }

    public RecycleBin() {
        super();
    }
}
