package com.huanledoudizhu.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import com.huanledoudizhu.config.BaseEntity;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author huanledoudizhu
 * @since 2022-11-21
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value="FileShare对象", description="")
public class FileShare extends BaseEntity<FileShare> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "共享文件md5")
    private String shareMd5;

    @ApiModelProperty(value = "共享文件id")
    private Integer fileId;

    @ApiModelProperty(value = "过期时间")
    private LocalDateTime expirationTime;

    @ApiModelProperty(value = "分享uuid")
    private String shareUuid;

    @ApiModelProperty(value = "文件夹分享压缩地址")
    private String shareLocation;

    @Override
    protected Serializable pkVal() {
        return null;
    }


}
