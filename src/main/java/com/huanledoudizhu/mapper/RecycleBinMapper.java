package com.huanledoudizhu.mapper;

import com.huanledoudizhu.entity.File;
import com.huanledoudizhu.entity.RecycleBin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author huanledoudizhu
 * @since 2022-11-13
 */
@Component
public interface RecycleBinMapper extends BaseMapper<RecycleBin> {
    @Select("SELECT * FROM recycle_bin WHERE creator_id=#{creator_id}")
    List<RecycleBin> getByCreatorId(@Param("creator_id") int creator_id);

    @Delete("DELETE FROM recycle_bin where id = #{id}")
    Boolean deleteById(@Param("id") int id);

    @Select("SELECT * FROM recycle_bin WHERE id=#{id}")
    RecycleBin getById(@Param("id") int id);

    @Delete("DELETE FROM recycle_bin WHERE id = #{id} AND creator_id=#{creator_id}")
    void deleteByIdWhereCreatorId(@Param("id")Integer integer, @Param("creator_id")int parseInt);
}
