package com.huanledoudizhu.mapper;

import com.huanledoudizhu.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author huanledoudizhu
 * @since 2022-11-13
 */

@Component
public interface UserMapper extends BaseMapper<User> {
    @Select("SELECT * FROM user WHERE email=#{email} and PASSWORD=#{password}")
    User selectByEmailAndPassword(@Param("email") String email,@Param("password") String password);
    @Select("SELECT * FROM user WHERE email=#{email}")
    User selectByEmail(@Param("email") String email);
}
