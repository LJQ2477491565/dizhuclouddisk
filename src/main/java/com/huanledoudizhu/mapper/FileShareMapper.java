package com.huanledoudizhu.mapper;

import com.huanledoudizhu.entity.FileShare;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author huanledoudizhu
 * @since 2022-11-21
 */
@Component
public interface FileShareMapper extends BaseMapper<FileShare> {
    @Select("SELECT * FROM file_share where share_uuid=#{uuid}")
    FileShare getByUUID(@Param("uuid") String uuid);
}
