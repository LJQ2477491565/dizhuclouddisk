package com.huanledoudizhu.mapper;

import com.huanledoudizhu.entity.UserSpace;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author huanledoudizhu
 * @since 2022-11-13
 */
public interface UserSpaceMapper extends BaseMapper<UserSpace> {

}
