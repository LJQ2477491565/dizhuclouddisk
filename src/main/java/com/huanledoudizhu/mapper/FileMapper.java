package com.huanledoudizhu.mapper;

import com.huanledoudizhu.entity.File;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huanledoudizhu.entity.User;
import io.swagger.v3.oas.annotations.Parameter;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author huanledoudizhu
 * @since 2022-11-13
 */
@Component
public interface FileMapper extends BaseMapper<File> {
    @Delete("DELETE FROM file where id = #{id}")
    Boolean deleteById(@Param("id") int id);
    @Update("UPDATE file SET file_name=#{file_name}, file_extension=#{file_extension}, file_location=#{location}, file_type=#{type} WHERE id=#{id}")
    Boolean UpdateFileNameAndFileExtensionById(@Param("file_name") String file_name, @Param("file_extension") String file_extension, @Param("location") String location,@Param("type") String type, @Param("id") int id);
    @Update("UPDATE file SET file_location=#{location} WHERE id=#{id}")
    Boolean UpdateFileLocationByFileId( @Param("location") String location,@Param("id") int id);
    @Select("SELECT * FROM file")
    List<File> getAll();
    @Select("SELECT * FROM file WHERE creator_id=#{creator_id}")
    List<File> getByCreatorId(@Param("creator_id") int creator_id);
    @Delete("DELETE FROM file WHERE id = #{id} AND creator_id=#{creator_id}")
    Boolean deleteByIdWhereCreatorId(@Param("id") int id,@Param("creator_id") int creator_id);
    @Select("SELECT * FROM file WHERE file_type=#{type} AND modifier_id=#{id}")
    List<File> getByFileType(@Param("type") String type, @Param("id") int id);
}
