package com.huanledoudizhu.controller.result;

public class ResponseResult {
    private Integer code;
    private String message;
    private Object data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public ResponseResult(com.huanledoudizhu.controller.result.ResultCode resultCode, Object data){
        this.code = resultCode.code();
        this.message = resultCode.message();
        this.data = data;
    }

    public ResponseResult(com.huanledoudizhu.controller.result.ResultCode resultCode){
        this.code = resultCode.code();
        this.message = resultCode.message();
    }
}


