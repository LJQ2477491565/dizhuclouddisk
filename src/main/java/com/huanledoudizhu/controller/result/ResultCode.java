package com.huanledoudizhu.controller.result;

public enum ResultCode {

    FAILURE(0,"失败"),
    SUCESS(1,"成功"),
    PARAM_IS_INVALID(3,"参数无效"),
    PARAM_IS_BLANK(4,"参数为空"),
    PARAM_NOT_COMPLETE(5,"参数缺失"),
    USER_NOT_LOGGED_IN(6,"用户未登录，访问的路径需要验证，请登录"),
    USER_LOGIN_ERROR(7,"账号不存在或密码错误"),
    USER_ACCOUNT_FORBIDDEN(8,"账号已被禁用"),
    USER_NOT_EXIST(9,"用户不存在"),
    USER_HAS_EXISTED(10,"用户已存在"),
    GET_EMPTY(11,"查询无信息"),
    SYSTEM_ERR(12,"系统错误"),
    BUSINESS_ERR(14,"业务错误"),
    SQL_ERR(15,"数据库错误"),
    SYSTEM_UNKNOW_ERR(16,"未知错误"),
    VERFICATION_CODE_EXPIRED(17,"验证码过期"),
    VERFICATION_CODE_ERR(18,"验证码错误"),
    FILE_EXCEEDS_SIZE(19,"文件太大"),
    ERFICATION_EAL_ERROR(20,"未验证邮箱");



    private Integer code;
    private String message;

    ResultCode(Integer code,String message){
        this.code = code;
        this.message = message;
    }

    public Integer code(){
        return this.code;
    }

    public String message(){
        return this.message;
    }

}
