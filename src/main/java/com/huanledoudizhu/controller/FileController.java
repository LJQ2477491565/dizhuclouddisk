package com.huanledoudizhu.controller;


import com.huanledoudizhu.controller.result.ResponseResult;
import com.huanledoudizhu.controller.result.ResultCode;
import com.huanledoudizhu.service.FileService;
import io.swagger.annotations.ApiOperation;
import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author huanledoudizhu
 * @since 2022-11-13
 */
@RestController
@RequestMapping("/file")
public class FileController {
    @Autowired
    FileService fileService;

    @ApiOperation(value = "上传文件")
    @PostMapping("upload")
    public ResponseResult upload(MultipartFile file, String currentDirectory) throws IOException {
        return fileService.upload(file,currentDirectory);
    }

    @ApiOperation(value = "下载文件")
    @PostMapping("download")
    public ResponseResult download (int id, HttpServletResponse httpServletResponse, HttpServletRequest httpServletRequest) throws IOException {
        return fileService.download(id, httpServletResponse, httpServletRequest);
    }

    @ApiOperation(value = "批量删除")
    @PostMapping("deleteBox")
    public ResponseResult delete(@RequestBody List<Integer> checkbox, String currentDirectory, HttpServletRequest httpServletRequest) throws JSONException {
        return fileService.delete(checkbox);
    }

    @ApiOperation(value = "创建目录")
    @PostMapping("createDir")
    public ResponseResult createDir(String dirName, String currentDirectory) throws JSONException {
        return fileService.createDir(dirName, currentDirectory);
    }

    @ApiOperation(value = "修改文件名称")
    @PostMapping("modifyFileName")
    public ResponseResult modifyFileName(int id, String filename, String currentDirectory) throws JSONException, IOException {
        return fileService.modifyFileName(id, filename, currentDirectory);
    }

    @ApiOperation(value = "获取当先用户当前目录所有文件")
    @PostMapping("getAllByCreatorId")
    public ResponseResult getAllByCreatorId(String currentDirectory) throws JSONException, IOException {
        return fileService.currentDirectory(currentDirectory);
    }

    @ApiOperation(value = "文件分类")
    @PostMapping("/classification")
    public ResponseResult classification(String c) throws JSONException, IOException {
        return fileService.classification(c);
    }
}
