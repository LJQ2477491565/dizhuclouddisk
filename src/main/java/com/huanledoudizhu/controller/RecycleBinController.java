package com.huanledoudizhu.controller;


import com.huanledoudizhu.controller.result.ResponseResult;
import com.huanledoudizhu.service.RecycleBinService;
import io.swagger.annotations.ApiOperation;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author huanledoudizhu
 * @since 2022-11-13
 */
@RestController
@RequestMapping("/recycle-bin")
public class RecycleBinController {
    @Autowired
    RecycleBinService recycleBinService;


    @ApiOperation(value = "文件移到回收站")
    @PostMapping("/deleteToRecycleBin")
    public ResponseResult deleteToRecycleBin(@RequestBody List<Integer> checkbox) throws JSONException {
        return recycleBinService.deleteToRecycleBin(checkbox);
    }

    @ApiOperation(value = "恢复文件")
    @PostMapping("/recoverFromRecycleBin")
    public ResponseResult recoverFromRecycleBin(@RequestBody List<Integer> checkbox){
        return recycleBinService.recoverFromRecycleBin(checkbox);
    }

    @ApiOperation(value = "获取当前用户回收站")
    @PostMapping("/getDirectory")
    public ResponseResult getDirectory(){
        return recycleBinService.getDirectory();
    }


    @ApiOperation(value = "删除回收站")
    @PostMapping("/delete")
    public ResponseResult delete(@RequestBody List<Integer> checkbox) {
        return recycleBinService.delete(checkbox);
    }
}
