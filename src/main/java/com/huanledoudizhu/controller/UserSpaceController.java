package com.huanledoudizhu.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author huanledoudizhu
 * @since 2022-11-13
 */
@RestController
@RequestMapping("/user-space")
public class UserSpaceController {

}
