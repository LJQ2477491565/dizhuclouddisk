package com.huanledoudizhu.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huanledoudizhu.controller.result.ResponseResult;
import com.huanledoudizhu.controller.result.ResultCode;
import com.huanledoudizhu.entity.User;
import com.huanledoudizhu.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author huanledoudizhu
 * @since 2022-11-13
 */

@RestController
@RequestMapping("/user")
@Api(value = "User对象",tags = "用户信息")
public class UserController {
    @Autowired
    private UserService userService;
    /*
    *
    * 应该是说自动装配了一个那个实现类
    * */

    //管理员接口
    //----------------------------------------------------------------
    @ApiOperation(value = "查询")
    @GetMapping(value = "/{id}")
    public ResponseResult get(@PathVariable Integer id) {
        return new ResponseResult(ResultCode.SUCESS,userService.getById(id));

        //1、枚举类型
        //调用mb自带的搜索id的方法
    }

    @GetMapping
    @ApiOperation("分页查询")
    public ResponseResult list(@RequestParam(defaultValue = "1") Integer current,
                               @RequestParam(defaultValue = "10") Integer size) {
        //
        return new ResponseResult(ResultCode.SUCESS,userService.page(new Page<>(current,size)));
    }

    @ApiOperation(value = "新增")
    @PostMapping
    public ResponseResult add(@RequestBody User user) {
        return new ResponseResult(ResultCode.SUCESS,userService.save(user));
    }

    @ApiOperation(value = "修改")
    @PutMapping
    public ResponseResult modify(@RequestBody User user) {
        return new ResponseResult(ResultCode.SUCESS,userService.updateById(user));
    }

    @ApiOperation(value = "删除")
    @DeleteMapping(value = "/{id}")
    public ResponseResult remove(@PathVariable Integer id) {
        return new ResponseResult(ResultCode.SUCESS,userService.removeById(id));
    }
    //----------------------------------------------------------------
    @ApiOperation(value = "注册")
    @PostMapping(value = "/register", produces = {"application/json;charset=UTF-8"})
    public ResponseResult register(@RequestBody @Valid User user, @Parameter int code, BindingResult bindingResult, HttpServletRequest httpServletRequest) {
        return userService.register(user, code, bindingResult);
    }
    //----------------------------------------------------------------
    @ApiOperation(value = "登录")
    @PostMapping(value = "/login", produces = {"application/json;charset=UTF-8"})
    public ResponseResult login(@RequestBody @Valid User user, BindingResult bindingResult, HttpServletRequest httpServletRequest) {
        return userService.login(user, bindingResult);
    }

    //1.
    @ApiOperation(value = "邮箱验证码")
    @PostMapping(value = "/email", produces = {"application/json;charset=UTF-8"})
    public ResponseResult sendCodeToEmail(@RequestBody @Valid User user, BindingResult bindingResult, HttpServletRequest httpServletRequest) {
        return userService.sendCodeToEmail(user, bindingResult);
    }

// 测试接口
//    @ApiOperation(value = "验证验证码")
//    @PostMapping(value = "/verification", produces = {"application/json;charset=UTF-8"})
//    public ResponseResult verification(int code) {
//        return userService.verification(code);
//    }

    //3.
    @ApiOperation(value = "通过验证码登录")
    @PostMapping(value = "/emailLogin", produces = {"application/json;charset=UTF-8"})
    public ResponseResult emailLogin(@RequestBody User user, int code) {
        return userService.emailLogin(user,code);
    }

    public static String getCliectIp(HttpServletRequest request)
    {
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.trim().equals("") || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.trim().equals("") || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.trim().equals("") || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }

        // 多个路由时，取第一个非unknown的ip
        final String[] arr = ip.split(",");
        for (final String str : arr) {
            if (!"unknown".equalsIgnoreCase(str)) {
                ip = str;
                break;
            }
        }
        return ip;
    }

}
