package com.huanledoudizhu.controller;


import com.huanledoudizhu.controller.result.ResponseResult;
import com.huanledoudizhu.service.FileService;
import com.huanledoudizhu.service.FileShareService;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.annotations.Param;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.Format;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author huanledoudizhu
 * @since 2022-11-21
 */
@RestController
@RequestMapping("/fileShare")
public class FileShareController {
    @Autowired
    FileShareService fileShareService;


    @ApiOperation("生成分享链接")
    @PostMapping("/generateLink")
    public ResponseResult generateLink(@Param("id") int id, @DateTimeFormat(pattern = "yyyy-MM-dd") Date expirationTime) throws IOException {
        return fileShareService.generateLink(id,expirationTime);
    }

    @ApiOperation("下载分享链接")
    @PostMapping("/linkDownload")
    public ResponseResult linkDownload(@Param("uuid") String uuid, HttpServletResponse httpServletResponse, HttpServletRequest httpServletRequest) throws IOException {
        return fileShareService.linkDownload(uuid, httpServletResponse, httpServletRequest);
    }

}
