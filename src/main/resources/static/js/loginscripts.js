// Empty JS for your own code to be here

console.log("页面加载完毕");
function passCheck() { //验证密码
    var userpass = document.form.password.value;
    if (userpass == "") {
        alert("未输入密码 \n" + "请输入密码");
        document.myform.password.focus();
        return false;
    }
    if (userpass.length < 6 || userpass.length > 12) {
        alert("密码必须在 6-12 个字符。\n");
        return false;
    }
    return true;
}
function selectEmailLogin(){
    var email = document.getElementById("email");
    var password = document.getElementById("password");
    var verification = document.getElementById("verification");
    var selectPassword = document.getElementById("selectPassword");
    var selectEmal = document.getElementById("selectEmal");
    var login_submit = document.getElementById("login-submit");
    var verification_login_submit = document.getElementById("verification-login-submit");
    var send_email = document.getElementById("send_email");
    password.style.display = "none";
    verification.style.display = "inline-block";
    selectPassword.style.display = "inline-block";
    selectEmal.style.display = "none";
    verification_login_submit.style.display = "inline-block";
    login_submit.style.display = "none";
    send_email.style.display = "inline-block";
}
function selectPassword(){
    var email = document.getElementById("email");
    var password = document.getElementById("password");
    var verification = document.getElementById("verification");
    var selectPassword = document.getElementById("selectPassword");
    var selectEmal = document.getElementById("selectEmal");
    var login_submit = document.getElementById("login-submit");
    var verification_login_submit = document.getElementById("verification-login-submit");
    var send_email = document.getElementById("send_email");
    password.style.display = "inline-block";
    verification.style.display = "none";
    selectPassword.style.display = "none";
    selectEmal.style.display = "inline-block";
    verification_login_submit.style.display = "none";
    login_submit.style.display = "inline-block";
    send_email.style.display = "none";
}
function checkEmail() {
    var Email = document.getElementById("email").value;
    var e = Email.indexOf("@" && ".");
    if (Email.length != 0) {
        if (e > 0) {
            if (Email.charAt(0) == "@" && ".") {
                alert("符号@和符号.不能再邮件地址第一位");
                return false;
            }
            else {
                return true;
            }
        }
        else {
            alert("电子邮件格式不正确\n" + "必须包含@符号和.符号！");
            return false;
        }
    }
    else {
        alert("请输入电子邮件！");
        return false;
    }
}
function login()
{
    var email = document.getElementById("email");
    var password = document.getElementById("password");
    var json = {"username": "空空","password": password.value,"email":email.value};
    var button = document.getElementById("login-submit");
    if (!checkEmail()||!passCheck()){
        return;
    }
    button.disabled = true;
    button.innerText="登录中...";
    var jsonstr = JSON.stringify(json);
    var xmlhttp;
    if (window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else
    {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP")
    }
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState===4 && xmlhttp.status===200)
        {
            var id = document.getElementById("myDiv");
            var responseJS = JSON.parse(xmlhttp.response);
            console.log(responseJS.code)
            if (responseJS.code !== 1)
                document.getElementById("inhint").innerText = "密码或账号错误";
            else
                window.location.href="Email.html";
            // button.innerText="登录";
            button.innerHTML="登录<i class=\"m-icon-swapright m-icon-white\"></i>";
            button.disabled = false;
        }
        else if (xmlhttp.readyState === 4)
        {
            console.log("响应失败");
            console.log(xmlhttp.status);
            // button.innerText="登录";
            button.innerHTML="登录<i class=\"m-icon-swapright m-icon-white\"></i>";
            button.disabled = false;
        }
    }

    xmlhttp.open("Post","user/login",true);
    xmlhttp.setRequestHeader("Content-type","application/json");
    xmlhttp.send(jsonstr);
}